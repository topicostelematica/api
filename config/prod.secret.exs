use Mix.Config

# In this file, we keep production configuration that
# you likely want to automate and keep it away from
# your version control system.
#
# You should document the content of this
# file or create a script for recreating it, since it's
# kept out of version control and might be hard to recover
# or recreate for your teammates (or you later on).
config :phoenix_graphql, PhoenixGraphql.Endpoint,
  secret_key_base: "MqFQ1FTibUxA7BzB8i/itv5XpwCdHU8FmD+iwM4jWmPq/qAhBFxpz/Xe0XVigpah"

# Configure your database
config :phoenix_graphql, PhoenixGraphql.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "db123456",
  database: "bdrdemo",
  hostname: "10.131.137.191",
  port: 15432,
  pool_size: 40
