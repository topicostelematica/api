defmodule PhoenixGraphql.Router do
  use PhoenixGraphql.Web, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :graphql do
    plug Guardian.Plug.VerifyHeader, realm: "Bearer"
    plug Guardian.Plug.LoadResource
    plug PhoenixGraphql.Web.Context
  end

  scope "/api" do
    pipe_through :graphql

    forward "/", Absinthe.Plug,
      schema: PhoenixGraphql.Schema
  end

  forward "/graphiql", Absinthe.Plug.GraphiQL,
    schema: PhoenixGraphql.Schema

end