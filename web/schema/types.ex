defmodule PhoenixGraphql.Schema.Types do
  use Absinthe.Schema.Notation
  use Absinthe.Ecto, repo: PhoenixGraphql.Repo

  @desc "Usuario de la plataforma"
  object :user do
    field :id, :id
    field :name, :string
    field :email, :string
    field :materias, list_of(:materia), resolve: assoc(:materias)
  end

  @desc "Materia, perteneciente a un usuario"
  object :materia do
    field :id, :id
    field :title, :string
    field :user, :user, resolve: assoc(:user)
    field :notas, list_of(:nota), resolve: assoc(:notas)
  end

  @desc "Nota, de una materia"
  object :nota do
    field :id, :id
    field :desc, :string
    field :valor, :float
    field :porcentaje, :float
    field :materia, :materia, resolve: assoc(:materia)
  end

  @desc "Sesión de un usuario"
  object :session do
    field :token, :string
  end

end