defmodule PhoenixGraphql.MateriaResolver do
  import Ecto.Query, only: [where: 2]

  alias PhoenixGraphql.Repo
  alias PhoenixGraphql.Materia

  def all(_args, %{context: %{current_user: %{id: user_id}}}) do
    materias =
      Materia
      |> where(user_id: ^user_id)
      |> Repo.all

    {:ok, materias}
  end

  def one(%{id: id}, %{context: %{current_user: %{id: user_id}}}) do
    materias =
      Materia
      |> where(id: ^id)
      |> Repo.one
    {:ok, materias}
  end

  def all(_args, _info) do
    {:error, "Not Authorized"}
  end

  def create(args, _info) do
    %Materia{}
    |> Materia.changeset(args)
    |> Repo.insert
  end

  def update(%{id: id, materia: materia_params}, _info) do
    Repo.get!(Materia, id)
    |> Materia.changeset(materia_params)
    |> Repo.update
  end

  def delete(%{id: id}, _info) do
    materia = Repo.get!(Materia, id)
    Repo.delete(materia)
  end

end