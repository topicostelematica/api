defmodule PhoenixGraphql.NotaResolver do
  import Ecto.Query, only: [where: 2]

  alias PhoenixGraphql.Repo
  alias PhoenixGraphql.Nota

  def all(%{materia_id: materia_id}, %{context: %{current_user: %{id: user_id}}}) do
    materia =
      PhoenixGraphql.Materia
      |> where(id: ^materia_id)
      |> where(user_id: ^user_id)
      |> Repo.one

   unless materia do
     {:error, "Not Authorized"}
   end

    notas =
      Nota
      |>where(materia_id: ^materia_id)
      |> Repo.all

    {:ok, notas}
  end

  def all(_args, _info) do
    {:error, "Not Authorized"}
  end

  def create(args, _info) do
    IO.inspect args
    %Nota{}
    |> Nota.changeset(args)
    |> Repo.insert
  end

  def update(%{id: id, nota: nota_params}, _info) do
    Repo.get!(Nota, id)
    |> Nota.changeset(nota_params)
    |> Repo.update
  end

  def delete(%{id: id}, _info) do
    nota = Repo.get!(Nota, id)
    Repo.delete(nota)
  end

end