defmodule PhoenixGraphql.UserResolver do
  alias PhoenixGraphql.Repo
  alias PhoenixGraphql.User
  require Logger



  def all(_args, %{context: %{current_user: %{id: user_id}}}) do
    {:ok, Repo.get(User, user_id)}
  end

  def all(_args, _info) do
    {:error, "Not Authorized"}
  end

  def update(%{id: id, user: user_params}, _info) do
    Repo.get!(User, id)
    |> User.update_changeset(user_params)
    |> Repo.update
  end

  def login(params, _info) do

    {:ok, ldap_conn} = Exldap.open()
    #{:ok, ldap_conn} = Exldap.connect
    bind = "#{params.email}@Dis.local"
    Logger.debug (bind)
    case Exldap.verify_credentials(ldap_conn, bind, params.password) do
      :ok ->
        Logger.debug ("Entre a ldap positivo")
        Logger.debug (params.email)
        Logger.debug (params.password)
        case Repo.get_by(User, email: params.email) do
          nil ->
            Logger.debug ("User no encontrado")
            case register(params, _info) do
              {:ok, user} ->
                with {:ok, user} <- PhoenixGraphql.Session.authenticate(params, Repo),
                  {:ok, jwt, _ } <- Guardian.encode_and_sign(user, :access) do
                  {:ok, %{token: jwt}}
                end
              {:error, changeset} ->
                {:error, "Error inserting"}
            end
          user ->
            Logger.debug ("User encontrado")
            #Logger.debug (user)
            with {:ok, user} <- PhoenixGraphql.Session.authenticate(params, Repo),
              {:ok, jwt, _ } <- Guardian.encode_and_sign(user, :access) do
              {:ok, %{token: jwt}}
            end
          end
      _ -> {:error, "Invalid username / password"}
    end

  end

  def register(args, _info) do
    %User{}
    |> User.registration_changeset(args)
    |> Repo.insert
  end


end