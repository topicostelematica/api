defmodule PhoenixGraphql.Schema do
  use Absinthe.Schema
  import_types PhoenixGraphql.Schema.Types

  query do
    @desc "Obtiene todas las Notas de una materia"
    field :notas, list_of(:nota) do
      arg :materia_id, non_null(:integer)
      resolve &PhoenixGraphql.NotaResolver.all/2
    end

    @desc "Obtiene todas las materias del current_user"
    field :materias, list_of(:materia) do
      resolve &PhoenixGraphql.MateriaResolver.all/2
    end

    @desc "Obtiene una materia por su ID"
    field :materia, type: :materia do
      arg :id, non_null(:integer)
      resolve &PhoenixGraphql.MateriaResolver.one/2
    end

    @desc "Obtiene un usuario (current_user)"
    field :user, type: :user do
      resolve &PhoenixGraphql.UserResolver.all/2
    end
  end

  input_object :update_user_params do
    field :name, :string
    field :email, :string
    field :password, :string
  end
  input_object :update_materia_params do
    field :title, non_null(:string)
    field :user_id, non_null(:integer)
  end
  input_object :update_nota_params do
    field :desc, non_null(:string)
    field :valor, non_null(:float)
    field :porcentaje, non_null(:float)
    field :materia_id, non_null(:integer)
  end

  mutation do
    @desc "Actualiza un usuario"
    field :update_user, type: :user do
      arg :id, non_null(:integer)
      arg :user, :update_user_params

      resolve &PhoenixGraphql.UserResolver.update/2
    end

    @desc "Inicio de sesión"
    field :login, type: :session do
      arg :email, non_null(:string)
      arg :password, non_null(:string)

      resolve &PhoenixGraphql.UserResolver.login/2
    end

    @desc "Registro / Crear usuario"
    field :register, type: :user do
      arg :email, non_null(:string)
      arg :password, non_null(:string)

      resolve &PhoenixGraphql.UserResolver.register/2
    end

    @desc "Crear una materia"
    field :create_materia, type: :materia do
        arg :title, non_null(:string)
        arg :user_id, non_null(:integer)

        resolve &PhoenixGraphql.MateriaResolver.create/2
    end

    @desc "Actualiza una materia"
    field :update_materia, type: :materia do
      arg :id, non_null(:integer)
      arg :materia, :update_materia_params

      resolve &PhoenixGraphql.MateriaResolver.update/2
    end

    @desc "Eliminar una materia"
    field :delete_materia, type: :materia do
      arg :id, non_null(:integer)

      resolve &PhoenixGraphql.MateriaResolver.delete/2
    end

    @desc "Crear una nota"
    field :create_nota, type: :nota do
        arg :desc, non_null(:string)
        arg :valor, non_null(:float)
        arg :porcentaje, non_null(:float)
        arg :materia_id, non_null(:integer)

        resolve &PhoenixGraphql.NotaResolver.create/2
    end

    @desc "Actualiza una nota"
    field :update_nota, type: :nota do
      arg :id, non_null(:integer)
      arg :nota, :update_nota_params

      resolve &PhoenixGraphql.NotaResolver.update/2
    end

    @desc "Eliminar una nota"
    field :delete_nota, type: :nota do
      arg :id, non_null(:integer)

      resolve &PhoenixGraphql.NotaResolver.delete/2
    end


  end

end