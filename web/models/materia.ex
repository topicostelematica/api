defmodule PhoenixGraphql.Materia do
  use PhoenixGraphql.Web, :model

  schema "materias" do
    field :title, :string
    belongs_to :user, PhoenixGraphql.User
    has_many :notas, PhoenixGraphql.Nota

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:title, :user_id])
    |> validate_required([:title, :user_id])
  end
end
