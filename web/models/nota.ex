defmodule PhoenixGraphql.Nota do
  use PhoenixGraphql.Web, :model

  schema "notas" do
    field :desc, :string
    field :valor, :decimal
    field :porcentaje, :decimal
    belongs_to :materia, PhoenixGraphql.Materia

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:desc, :valor, :porcentaje, :materia_id])
    |> validate_required([:desc, :valor, :porcentaje, :materia_id])
  end
end
