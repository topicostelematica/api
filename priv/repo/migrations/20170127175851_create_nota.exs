defmodule PhoenixGraphql.Repo.Migrations.CreateNota do
  use Ecto.Migration

  def change do
    create table(:notas) do
      add :valor, :decimal
      add :porcentaje, :decimal
      add :materia_id, references(:materias, on_delete: :delete_all)

      timestamps()
    end
    create index(:notas, [:materia_id])

  end
end
