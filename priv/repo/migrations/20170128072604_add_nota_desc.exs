defmodule PhoenixGraphql.Repo.Migrations.AddNotaDesc do
  use Ecto.Migration

  def change do
    alter table(:notas) do
      add :desc, :string
    end
  end
end
