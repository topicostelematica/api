defmodule PhoenixGraphql.Repo.Migrations.CreateMateria do
  use Ecto.Migration

  def change do
    create table(:materias) do
      add :title, :string
      add :user_id, references(:users, on_delete: :delete_all)

      timestamps()
    end
    create index(:materias, [:user_id])

  end
end
