# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     PhoenixGraphql.Repo.insert!(%PhoenixGraphql.SomeModel{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias PhoenixGraphql.User
alias PhoenixGraphql.Materia
alias PhoenixGraphql.Nota
alias PhoenixGraphql.Repo

# Usuarios
Repo.insert!(%User{name: "Cristian Ospina", email: "ospina@ospina.com"})
Repo.insert!(%User{name: "Rosie", email: "rosie@mydog.com"})

# Materias
for _ <- 1..10 do
  Repo.insert!(%Materia{
    title: Faker.Company.name,
    user_id: [1, 2] |> Enum.take_random(1) |> hd
  })
end

# Notas
Repo.insert!(%Nota{desc: "Parcial 1", valor: 4, porcentaje: 25, materia_id: 1})
Repo.insert!(%Nota{desc: "Parcial 2", valor: 3, porcentaje: 25, materia_id: 1})
Repo.insert!(%Nota{desc: "Parcial 3", valor: 4, porcentaje: 25, materia_id: 1})
Repo.insert!(%Nota{desc: "Parcial 4", valor: 3, porcentaje: 25, materia_id: 1})

Repo.insert!(%Nota{desc: "Parcial 1", valor: 4, porcentaje: 25, materia_id: 2})
Repo.insert!(%Nota{desc: "Parcial 2", valor: 3, porcentaje: 25, materia_id: 2})
Repo.insert!(%Nota{desc: "Parcial 3", valor: 4, porcentaje: 25, materia_id: 2})
Repo.insert!(%Nota{desc: "Parcial 4", valor: 3, porcentaje: 25, materia_id: 2})