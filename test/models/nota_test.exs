defmodule PhoenixGraphql.NotaTest do
  use PhoenixGraphql.ModelCase

  alias PhoenixGraphql.Nota

  @valid_attrs %{porcentaje: "120.5", valor: "120.5"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Nota.changeset(%Nota{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Nota.changeset(%Nota{}, @invalid_attrs)
    refute changeset.valid?
  end
end
