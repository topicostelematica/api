defmodule PhoenixGraphql.MateriaTest do
  use PhoenixGraphql.ModelCase

  alias PhoenixGraphql.Materia

  @valid_attrs %{title: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Materia.changeset(%Materia{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Materia.changeset(%Materia{}, @invalid_attrs)
    refute changeset.valid?
  end
end
